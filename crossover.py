# -*- coding: utf-8 -*-
from avaliacao import avaliar_individuos
from avaliacao import avalia_individuo
from random import choice
from random import sample
from random import shuffle
from random import random
from dados import monta_individuo_completo
from config import INDIVIDUOS
from config import ITERACOES

class Crossover(object):

    def __init__(self):
        self.individuos = avaliar_individuos()
        self.individuos.sort(key=lambda item: item["avaliacao"],reverse=True)
        self.pai1 = []
        self.pai2 = []

    def sorteio(self):
        for i in range(0,ITERACOES):
            individuos_copy = list(self.individuos)
            while len(individuos_copy) > 3:
                sorteados = sample(individuos_copy, 4)
                shuffle(sorteados)
                primeiro = [sorteados[0], sorteados[1]]
                segundo = [sorteados[2], sorteados[3]]

                self.pai1 = primeiro[0] if primeiro[0]['avaliacao'] > primeiro[1]['avaliacao'] else primeiro[1]
                self.pai2 = segundo[0] if segundo[0]['avaliacao'] > segundo[1]['avaliacao'] else segundo[1]

                #remove os 2 sorteados
                individuos_copy.remove(self.pai1)
                individuos_copy.remove(self.pai2)

                #cria os novos filhos
                filho1, filho2 = self.crossover_ciclo()
                #mutacao
                if random() > 0.9:
                    mutacao = sample([filho1,filho2],1)[0]
                    for linha in mutacao:
                        shuffle(linha)
                    
                #cria os filhos completos para avaliacao
                filho1_completo = monta_individuo_completo(filho1)
                filho2_completo = monta_individuo_completo(filho2)
                #avalia os novos filhos
                avaliacao1 = avalia_individuo(filho1_completo)
                avaliacao2 = avalia_individuo(filho2_completo)
                #adiciona os filhos na populacao
                self.individuos.append({"individuo":filho1, "individuo_completo":filho1_completo, "avaliacao":avaliacao1})
                self.individuos.append({"individuo":filho2, "individuo_completo":filho2_completo, "avaliacao":avaliacao2})

            #acasala os dois que sobraram
            self.pai1 = individuos_copy.pop()
            self.pai2 = individuos_copy.pop()
            #cria os novos filhos
            filho1, filho2 = self.crossover_ciclo()
            #cria os filhos completos para avaliacao
            filho1_completo = monta_individuo_completo(filho1)
            filho2_completo = monta_individuo_completo(filho2)
            #avalia os novos filhos
            avaliacao1 = avalia_individuo(filho1_completo)
            avaliacao2 = avalia_individuo(filho2_completo)
            #adiciona os filhos na populacao
            self.individuos.append({"individuo":filho1, "individuo_completo":filho1_completo, "avaliacao":avaliacao1})
            self.individuos.append({"individuo":filho2, "individuo_completo":filho2_completo, "avaliacao":avaliacao2})

            #ordena os individuos
            self.individuos.sort(key=lambda item: item["avaliacao"],reverse=True)
            #remove os piores
            for apagar in range(0,INDIVIDUOS):
                self.individuos.pop()
            if self.individuos[0]['avaliacao'] == 243:
                break
        
        print "MELHOR RESULTADO: %s" % self.individuos[0]['avaliacao']
        for linha in self.individuos[0]['individuo_completo']:
            print linha
        print "ITERACOES: %s" % (i + 1)

        return True

    def crossover_ciclo(self):
        pai1_copy = list(self.pai1['individuo'])
        pai2_copy = list(self.pai2['individuo'])

        #for para cada linha
        individuo_filho1 = []
        individuo_filho2 = []
        for idx, linha in enumerate(pai1_copy):
            inicio = -1
            linha2 = pai2_copy[idx]
            len_de_linha = len(linha)
            filho1 = [0 for i in range(0,len_de_linha)]
            filho2 = [0 for i in range(0,len_de_linha)]
            for i in range(0,len_de_linha):
                dio = choice(range(0,len_de_linha))
                if linha[dio] != linha2[dio]:
                    inicio = dio
                    break

            #aqui inicia o ciclo
            #pegar a linha e guardar indece para pegar a mesma linha no pai dois
            if inicio != -1:
                fim_ciclo = linha[inicio]
                caminho = []
                for j in range(0,len_de_linha):
                    caminho.append(inicio)
                    valor_pai2 = linha2[inicio]
                    pos_pai1 = linha.index(valor_pai2)
                    if linha2[pos_pai1] == fim_ciclo:
                        caminho.append(pos_pai1)
                        break
                    inicio = pos_pai1

                #achei o ciclo. montar os filhos.
                for i in caminho:
                    filho1[i] = linha[i]    
                    filho2[i] = linha2[i]

                while 0 in filho1:
                    filho1[filho1.index(0)] = linha2[filho1.index(0)]

                while 0 in filho2:
                    filho2[filho2.index(0)] = linha[filho2.index(0)]

#                print fim_ciclo
#                print linha
#                print linha2
#                print filho1
#                print filho2
#                print
                individuo_filho1.append(filho1)
                individuo_filho2.append(filho2)
            else:
                individuo_filho1.append(linha)
                individuo_filho2.append(linha2)

        return individuo_filho1, individuo_filho2


#   def crossover_uniforme(self):
#        filho1 = []
#        mascara = []
#        for i in range(0,46):
#            mascara.append(choice([0,1]))
#         
#        for i,m in enumerate(mascara):
#            if m:
#                filho1.append(pai1_copy[i])
#                pai1_copy[i]=0
#                continue
#            filho1.append(pai2_copy[i])
#            pai2_copy[i]=0

if __name__ == "__main__":
    Crossover().sorteio()

