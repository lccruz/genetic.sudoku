# -*- coding: utf-8 -*-
from dados import monta_individuos

def avaliar_individuos():
    resultado = []
    individuos, individuos_completos = monta_individuos()
    for idx, individuo_completo in enumerate(individuos_completos):
        avaliacao = avalia_individuo(individuo_completo)
        resultado.append({"individuo": individuos[idx], 
                          "individuo_completo": individuo_completo, 
                          "avaliacao": avaliacao,})
    return resultado

def avalia_individuo(individuo):
    """Soma +1 para cada numero de linha, coluna ou regiao que estiver ok
       inicia com 81 pois as 9 linhas estao ok
    """
    soma = 81
    #monta as colunas
    colunas = []
    for i in range(0,9):
        colunas.append([linha[i] for linha in individuo])
    #avalia as colunas
    for coluna in colunas:
        soma += len(set(coluna))
    #monta as regioes
    individuo_unico = []
    for linha in individuo:
        for num in linha:
            individuo_unico.append(num)
    regioes = []
    for i in range(3): # linha maior
        for j in range(3): # coluna maior
            regiao = []
            for k in range(3): # linha menor
                for l in range(3): # coluna menor
                    regiao.append(individuo_unico[i*27 + j * 3 + k * 9 + l])
            regioes.append(regiao)
    #avalia as regioes
    for regiao in regioes:
        soma += len(set(regiao))
    return soma 

if __name__=="__main__":
    avaliar_individuos()
