# -*- coding: utf-8 -*-
from config import INDIVIDUOS
from config import INICIAL
import random

def monta_individuos():
    lista = range(1,10)
    individuos = []
    individuos_completos = []
    for i in range(0,INDIVIDUOS):
        novo_individuo = []
        novo_individuo_completo = []
        for linha in INICIAL:
            nova_linha = []
            nova_linha_completa = list(linha)
            um_a_dez = list(lista)
            um_a_dez = list(set(um_a_dez) - set(linha))
            random.shuffle(um_a_dez)
            for j in um_a_dez:
                nova_linha.append(j)
                nova_linha_completa[nova_linha_completa.index(0)] = j
            novo_individuo.append(nova_linha)
            novo_individuo_completo.append(nova_linha_completa)
        individuos.append(novo_individuo)
        individuos_completos.append(novo_individuo_completo)
#    individuos_completos.append(
#        [[2,5,6,9,7,8,3,1,4],
#         [8,3,1,2,4,5,6,7,9],
#         [4,9,7,3,1,6,2,8,5],
#         [1,2,8,7,3,4,9,5,6],
#         [9,6,4,1,5,2,8,3,7],
#         [3,7,5,6,8,9,1,4,2],
#         [7,8,9,5,2,1,4,6,3],
#         [5,4,2,8,6,3,7,9,1],
#         [6,1,3,4,9,7,5,2,8],
#        ]
#        )
    return individuos, individuos_completos

def monta_individuo_completo(individuo):
    """
    """
    novo_individuo_completo = []
    for idx, linha in enumerate(INICIAL):
        nova_linha_completa = list(linha)
        for i in individuo[idx]:
            nova_linha_completa[nova_linha_completa.index(0)] = i

        novo_individuo_completo.append(nova_linha_completa)
    return novo_individuo_completo


#print "INDIVIDUOS"
#individuos, individuos_completos = monta_individuos()
#for dio in individuos:
#    for xita in dio:
#        print xita
#    print ""
#
#print ""
#print "INDIVIDUOS COMPLETOS"
#for dio in individuos_completos:
#    for xita in dio:
#        print xita
#    print ""
